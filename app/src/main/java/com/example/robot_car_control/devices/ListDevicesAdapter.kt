package com.example.robot_car_control.devices

import android.bluetooth.BluetoothDevice
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.robot_car_control.databinding.ItemListBluetoothDevicesBinding

class ListDevicesAdapter(
    private val viewModel: BluetoothDevicesListViewModel
) :  ListAdapter<BluetoothDevice, ListDevicesAdapter.ViewHolder>(TasksDiffCallback()) {

    class ViewHolder(val binding: ItemListBluetoothDevicesBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bindData(viewModel: BluetoothDevicesListViewModel, device : BluetoothDevice) {
            binding.viewModel = viewModel
            binding.device = device
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemListBluetoothDevicesBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListDevicesAdapter.ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ListDevicesAdapter.ViewHolder, position: Int) {
        holder.bindData(viewModel, getItem(position))
    }

}

/**
 * Callback for calculating the diff between two non-null items in a list.
 *
 * Used by ListAdapter to calculate the minimum number of changes between and old list and a new
 * list that's been passed to `submitList`.
 */
class TasksDiffCallback : DiffUtil.ItemCallback<BluetoothDevice>() {
    override fun areItemsTheSame(oldItem: BluetoothDevice, newItem: BluetoothDevice): Boolean {
        return oldItem.address == newItem.address
    }

    override fun areContentsTheSame(oldItem: BluetoothDevice, newItem: BluetoothDevice): Boolean {
        return oldItem == newItem
    }
}