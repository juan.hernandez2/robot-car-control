package com.example.robot_car_control.devices

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.robot_car_control.R
import com.example.robot_car_control.databinding.ActivityBluetoothDevicesBinding
import com.example.robot_car_control.util.replaceFragmentInActivity
import com.example.robot_car_control.util.setupActionBar
import kotlinx.android.synthetic.main.toolbar.*

class BluetoothDevicesListActivity : AppCompatActivity() {

    private lateinit var viewDataBinding: ActivityBluetoothDevicesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_bluetooth_devices)
        configureActionBar()
        setupFragment()
    }

    private fun setupFragment() {
        replaceFragmentInActivity(
        supportFragmentManager.findFragmentById(R.id.contentFrame)
                ?: BluetoothDevicesListFragment.newInstance().apply {
                    arguments = intent.extras
                }, R.id.contentFrame
        )
    }

    private fun configureActionBar() {
        setupActionBar(R.id.toolbar) {
            supportActionBar?.setDisplayShowTitleEnabled(false)
            toolbar_title.text = getString(R.string.titleDevicesActivity)
        }
    }
}