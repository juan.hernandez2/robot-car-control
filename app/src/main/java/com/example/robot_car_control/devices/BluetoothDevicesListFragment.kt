package com.example.robot_car_control.devices

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.robot_car_control.R
import com.example.robot_car_control.databinding.FragmentBluetoothDevicesBinding
import com.example.robot_car_control.di.AppComponentHolder
import com.example.robot_car_control.movementControl.MovementControlActivity
import javax.inject.Inject

const val EXTRA_DEVICE = "extra_device"

class BluetoothDevicesListFragment : Fragment() {

    @Inject
    lateinit var viewModel: BluetoothDevicesListViewModel
    private lateinit var viewDataBinding: FragmentBluetoothDevicesBinding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        AppComponentHolder.component.inject(this)
        return setDataBinding(inflater, container).root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        addObservers()
    }

    override fun onResume() {
        super.onResume()
        setupBluetoothAdapter()
    }

    private fun setDataBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentBluetoothDevicesBinding {
        viewDataBinding = FragmentBluetoothDevicesBinding.inflate(inflater, container, false).apply {
            viewModel = this@BluetoothDevicesListFragment.viewModel
            lifecycleOwner = this@BluetoothDevicesListFragment.viewLifecycleOwner
        }
        return viewDataBinding
    }

    private fun setupRecyclerView() {
        val recyclerView = viewDataBinding.recyclerViewDevices
        val listAdapter = ListDevicesAdapter(viewModel)
        recyclerView.adapter = listAdapter
    }

    private fun setupBluetoothAdapter() {
        val btAdapter =  BluetoothAdapter.getDefaultAdapter()
        if(btAdapter == null) {
            Toast.makeText(context, getString(R.string.bluetooth_unavailable_message), Toast.LENGTH_SHORT).show()
        } else {
            getPairedDevices(btAdapter)
        }
    }

    private fun getPairedDevices(btAdapter: BluetoothAdapter) {
        if (btAdapter.isEnabled) {
            viewModel.setDevices(btAdapter.bondedDevices.toList())
        } else {
            requestEnableBluetooth()
        }
    }

    private fun requestEnableBluetooth() {
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(enableBtIntent, 1)
    }

    private fun addObservers() {
        viewModel.openMainMenuEvent.observe(this, Observer {
            startMainMenuActivity(it)
        })
    }

    private fun startMainMenuActivity(device: BluetoothDevice) {
        val intent = Intent(context, MovementControlActivity::class.java)
        intent.putExtra(EXTRA_DEVICE, device)
        startActivity(intent)
    }

    companion object {
        @JvmStatic
        fun newInstance() = BluetoothDevicesListFragment()
        private val TAG = BluetoothDevicesListActivity::class.java.simpleName
    }

}
