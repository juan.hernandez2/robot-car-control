package com.example.robot_car_control.devices

import android.bluetooth.BluetoothDevice
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<BluetoothDevice>?) {
    if(items != null) {
        (listView.adapter as ListDevicesAdapter?)?.submitList(items)
    }
}