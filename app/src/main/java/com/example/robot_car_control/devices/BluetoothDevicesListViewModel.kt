package com.example.robot_car_control.devices

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class BluetoothDevicesListViewModel @Inject constructor() : ViewModel() {
    private val _items = MutableLiveData<List<BluetoothDevice>>()
    val items: LiveData<List<BluetoothDevice>> = _items

    private val _openMainMenuEvent = MutableLiveData<BluetoothDevice>()
    val openMainMenuEvent: LiveData<BluetoothDevice> = _openMainMenuEvent

    fun setDevices(devices: List<BluetoothDevice>) {
        _items.value = devices
    }

    fun openMainMenu(device: BluetoothDevice) {
        _openMainMenuEvent.value = device
    }
}