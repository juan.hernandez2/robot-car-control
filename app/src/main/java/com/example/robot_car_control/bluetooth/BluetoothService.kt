package com.example.robot_car_control.bluetooth

import android.bluetooth.BluetoothDevice

interface BluetoothService {
    fun startClient(device: BluetoothDevice?, startClientCallback: StartClientCallback)

    interface StartClientCallback {
        fun onStartConnection()
        fun onConnectionEstablished()
    }
}