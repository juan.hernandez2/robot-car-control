package com.example.robot_car_control.bluetooth

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.util.Log
import com.example.robot_car_control.bluetooth.BluetoothService.StartClientCallback
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.Charset
import java.util.*
import javax.inject.Inject

class BluetoothConnectionService @Inject constructor() : BluetoothService {

    private val bluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private var connectThread: ConnectThread? = null
    private var connectedThread: ConnectedThread? = null
    private var startClientCallback: StartClientCallback? = null

    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private inner class ConnectThread(private val device: BluetoothDevice?) : Thread() {

        private var socket: BluetoothSocket? = null

        override fun run() {
            Log.i(TAG, "RUN mConnectThread ")

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                Log.d(TAG, "ConnectThread: Trying to create an RfcommSocket using UUID: $MY_UUID_SSP")
                socket = device!!.createRfcommSocketToServiceRecord(MY_UUID_SSP)
            } catch (e: IOException) {
                Log.e(TAG, "ConnectThread: Could not create InsecureRfcommSocket " + e.message)
            }

            // Always cancel discovery because it will slow down a connection
            bluetoothAdapter.cancelDiscovery()

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                socket!!.connect()
                Log.d(TAG, "run: ConnectThread connected.")
            } catch (e: IOException) {
                // Close the socket
                try {
                    socket!!.close()
                    Log.d(TAG, "run: Closed Socket.")
                } catch (e1: IOException) {
                    Log.e(TAG, "mConnectThread: run: Unable to close connection in socket " + e1.message)
                }
                Log.d(TAG, "run: ConnectThread: Could not connect to UUID: $MY_UUID_SSP")
            }

            connected(socket)
        }

        fun cancel() {
            try {
                Log.d(TAG, "cancel: Closing Client Socket.")
                socket!!.close()
            } catch (e: IOException) {
                Log.e(TAG, "cancel: close() of mmSocket in Connectthread failed. " + e.message)
            }
        }

        init {
            Log.d(TAG, "ConnectThread: started.")
            startClientCallback?.onStartConnection()
        }
    }


    /**
     *ConnectThread starts and attempts to make a connection with the other devices AcceptThread.
     */
    override fun startClient(device: BluetoothDevice?, startClientCallback: StartClientCallback) {
        Log.d(TAG, "startClient:  .")

        this.startClientCallback = startClientCallback

        connectThread = ConnectThread(device)
        connectThread?.start()
    }

    fun cancel() {
        connectedThread?.cancel()
    }

    /**
     * Finally the ConnectedThread which is responsible for maintaining the BTConnection, Sending the data, and
     * receiving incoming data through input/output streams respectively.
     */
    private inner class ConnectedThread(private val socket: BluetoothSocket?) : Thread() {
        private var inStream: InputStream? = null
        private var outStream: OutputStream? = null

        override fun run() {
            val buffer = ByteArray(1024) // buffer store for the stream
            var bytes: Int // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                // Read from the InputStream
                try {
                    bytes = inStream!!.read(buffer)
                    val incomingMessage = String(buffer, 0, bytes)
                    Log.d(TAG, "InputStream: $incomingMessage")
                } catch (e: IOException) {
                    Log.e(TAG, "write: Error reading Input Stream. " + e.message)
                    break
                }
            }
        }

        //Call this from the main activity to send data to the remote device
        fun write(bytes: ByteArray?) {
            val text = String(bytes!!, Charset.defaultCharset())
            Log.d(TAG, "write: Writing to outputstream: $text")
            try {
                outStream!!.write(bytes)
            } catch (e: IOException) {
                Log.e(TAG, "write: Error writing to output stream. " + e.message)
            }
        }

        /* Call this from the main activity to shutdown the connection */
        fun cancel() {
            try {
                socket!!.close()
            } catch (e: IOException) {

            }
        }

        init {
            Log.d(TAG, "ConnectedThread: Starting.")

            startClientCallback?.onConnectionEstablished()

            try {
                inStream = socket!!.inputStream
                outStream = socket.outputStream
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun connected(mmSocket: BluetoothSocket?) {
        Log.d(TAG, "connected: Starting.")

        // Start the thread to manage the connection and perform transmissions
        connectedThread = ConnectedThread(mmSocket)
        connectedThread?.start()
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     * @see ConnectedThread.write
     */
    fun write(out: ByteArray?) {

        // Synchronize a copy of the ConnectedThread
        Log.d(TAG, "write: Write Called.")
        //perform the write
        if (connectedThread != null) {
            connectedThread!!.write(out)
        }
    }

    companion object {
        private const val TAG = "BluetoothConnectionServ"
        private const val APP_NAME = "robotCarControl"
        private val MY_UUID_SSP = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    }

}