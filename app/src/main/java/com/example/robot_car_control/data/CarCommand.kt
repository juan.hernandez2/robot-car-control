package com.example.robot_car_control.data

data class CarCommand(val operation: Int, val controlValue: Int?)