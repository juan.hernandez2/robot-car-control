package com.example.robot_car_control.data

enum class MovementType(val type: Int) {
    MOVE_FORWARD(101),
    MOVE_BACKWARD(102),
    MOVE_LEFT(104),
    MOVE_RIGHT(103),
    MOVE_STEP(106),
    MOVE_STOP(105)
}