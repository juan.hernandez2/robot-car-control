package com.example.robot_car_control.data

enum class ControlMode(val mode: Int) {
    MOVE_MODE(1),
    SPEED_MODE(2),
    ANGLE_POS(3),
    ANGLE_DIS(4),
    FOLLOW_MODE(5),
    OBSTACLE_AVOIDANCE(6)
}