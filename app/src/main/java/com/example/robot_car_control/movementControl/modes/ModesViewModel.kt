package com.example.robot_car_control.movementControl.modes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.robot_car_control.R
import com.example.robot_car_control.data.CarCommand
import com.example.robot_car_control.data.ControlMode
import com.example.robot_car_control.data.MovementType
import com.example.robot_car_control.util.Event
import javax.inject.Inject

class ModesViewModel @Inject constructor() : ViewModel() {

    private val _carCommand = MutableLiveData<CarCommand>()
    val carCommand: LiveData<CarCommand> = _carCommand

    fun sendTypeAction(modeType: Int) {
        if(modeType == ControlMode.MOVE_MODE.mode) {
            sendCommandData(modeType, MovementType.MOVE_STOP.type)
        } else {
            sendCommandData(modeType, null)
        }
    }

    private fun sendCommandData(command: Int, selectedAction: Int?) {
        _carCommand.value = CarCommand(command, selectedAction)
    }



}