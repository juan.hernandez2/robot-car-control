package com.example.robot_car_control.movementControl.rotation

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.robot_car_control.databinding.FragmentRotationBinding
import com.example.robot_car_control.di.AppComponentHolder
import com.example.robot_car_control.movementControl.BluetoothCommandListener
import com.google.gson.Gson
import javax.inject.Inject

class RotationFragment : Fragment() {

    @Inject
    lateinit var viewModel: RotationViewModel

    private lateinit var viewDataBinding: FragmentRotationBinding
    private var bluetoothCommandListener: BluetoothCommandListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is BluetoothCommandListener) {
            bluetoothCommandListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        bluetoothCommandListener = null
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        AppComponentHolder.component.inject(this)
        setDataBinding(inflater, container)
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addObservers()
    }

    private fun setDataBinding(inflater: LayoutInflater, container: ViewGroup?) {
        viewDataBinding = FragmentRotationBinding.inflate(inflater, container, false).apply {
            viewModel = this@RotationFragment.viewModel
            lifecycleOwner = this@RotationFragment.viewLifecycleOwner
        }
    }

    private fun addObservers() {
        viewModel.carCommand.observe(this, Observer {
            val jsonCommand = Gson().toJson(it)
            Log.d(RotationFragment.TAG, "Progress:$jsonCommand)")
            bluetoothCommandListener?.sendCommand(jsonCommand)
        })
    }

    companion object {
        @JvmStatic
        fun newInstance() = RotationFragment()
        private val TAG = RotationFragment::class.java.simpleName
    }
}