package com.example.robot_car_control.movementControl.rotation

import androidx.databinding.BindingAdapter
import com.sdsmdg.harjot.crollerTest.Croller
import com.sdsmdg.harjot.crollerTest.OnCrollerChangeListener


@BindingAdapter("app:onAngularProgressChanged")
fun setOnAngularProgressChanged(croller: Croller, viewModel: RotationViewModel) {
    croller.setOnCrollerChangeListener(object : OnCrollerChangeListener {
        override fun onProgressChanged(croller: Croller?, progress: Int) {}

        override fun onStartTrackingTouch(croller: Croller?) {}

        override fun onStopTrackingTouch(croller: Croller?) {
            croller?.let {
                viewModel.sendAngleSeekBarValue(-1*((croller.progress.toDouble()*3.6)-180).toInt())
            }
        }
    })
}


