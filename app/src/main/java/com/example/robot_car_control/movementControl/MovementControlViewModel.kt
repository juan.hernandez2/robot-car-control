package com.example.robot_car_control.movementControl

import android.bluetooth.BluetoothDevice
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.robot_car_control.R
import com.example.robot_car_control.bluetooth.BluetoothConnectionService
import com.example.robot_car_control.bluetooth.BluetoothService
import com.example.robot_car_control.util.Event
import com.google.android.material.bottomnavigation.BottomNavigationView
import javax.inject.Inject

class MovementControlViewModel @Inject constructor(
        private val bluetoothConnectionService: BluetoothConnectionService
) : ViewModel() {

    private val _openModeFragmentEvent = MutableLiveData<Event<Unit>>()
    val openModeFragmentEvent: LiveData<Event<Unit>> = _openModeFragmentEvent

    private val _openMovementFragmentEvent = MutableLiveData<Event<Unit>>()
    val openMovementFragmentEvent: LiveData<Event<Unit>> = _openMovementFragmentEvent

    private val _openRotationFragmentEvent = MutableLiveData<Event<Unit>>()
    val openRotationFragmentEvent: LiveData<Event<Unit>> = _openRotationFragmentEvent

    private val _isVisibleProgressBar = MutableLiveData<Boolean>()
    val isVisibleProgressBar: LiveData<Boolean> = _isVisibleProgressBar

    val onNavigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener =
            BottomNavigationView.OnNavigationItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.navigationModes -> {
                        _openModeFragmentEvent.value = Event(Unit)
                        true
                    }
                    R.id.navigationMovement -> {
                        _openMovementFragmentEvent.value = Event(Unit)
                        true
                    }
                    R.id.navigationRotation -> {
                        _openRotationFragmentEvent.value = Event(Unit)
                        true
                    }
                    else -> false
                }
            }

    fun startBluetoothConnectionService(device: BluetoothDevice?) {
        bluetoothConnectionService.startClient(device, object : BluetoothService.StartClientCallback{
            override fun onStartConnection() {
                _isVisibleProgressBar.postValue(true)
            }

            override fun onConnectionEstablished() {
                _isVisibleProgressBar.postValue(false)
            }
        })
    }
    
    fun stopBluetoothConnectionService() {
        bluetoothConnectionService.cancel()
    }

    fun sendBluetoothCommand(jsonData: String) {
        bluetoothConnectionService.write(jsonData.toByteArray())
    }

}