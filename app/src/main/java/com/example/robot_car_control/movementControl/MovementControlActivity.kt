package com.example.robot_car_control.movementControl

import android.app.ProgressDialog
import android.bluetooth.BluetoothDevice
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.robot_car_control.R
import com.example.robot_car_control.databinding.ActivityMovementControlBinding
import com.example.robot_car_control.devices.EXTRA_DEVICE
import com.example.robot_car_control.di.AppComponentHolder
import com.example.robot_car_control.movementControl.modes.ModesFragment
import com.example.robot_car_control.movementControl.movement.MovementFragment
import com.example.robot_car_control.movementControl.rotation.RotationFragment
import com.example.robot_car_control.util.EventObserver
import com.example.robot_car_control.util.replaceFragmentInActivity
import com.example.robot_car_control.util.setupActionBar
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class MovementControlActivity : AppCompatActivity(), BluetoothCommandListener {

    @Inject
    lateinit var viewModel: MovementControlViewModel

    private lateinit var viewDataBinding: ActivityMovementControlBinding
    private var device: BluetoothDevice? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponentHolder.component.inject(this)
        setDataBinding()
        configureActionBar()
        addObservers()
        setupFragment()
        setUpBluetoothDevice()

    }

    override fun onResume() {
        super.onResume()
        viewModel.startBluetoothConnectionService(device)
    }

    override fun onPause() {
        super.onPause()
        viewModel.stopBluetoothConnectionService()
    }

    private fun setDataBinding() {
        viewDataBinding =
                DataBindingUtil.setContentView<ActivityMovementControlBinding>(this, R.layout.activity_movement_control) .apply {
                    viewModel = this@MovementControlActivity.viewModel
                    lifecycleOwner = this@MovementControlActivity
                }
    }

    private fun setupFragment() {
        launchFragment(MovementFragment.newInstance())
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setUpBluetoothDevice() {
        device = intent.extras?.getParcelable(EXTRA_DEVICE)
    }

    private fun addObservers() {
        viewModel.openModeFragmentEvent.observe(this, EventObserver {
            launchFragment(ModesFragment.newInstance())
        })
        viewModel.openMovementFragmentEvent.observe(this, EventObserver {
            launchFragment(MovementFragment.newInstance())
        })
        viewModel.openRotationFragmentEvent.observe(this, EventObserver {
            launchFragment(RotationFragment.newInstance())
        })
    }

    private fun launchFragment(fragment: Fragment) {
        replaceFragmentInActivity(
             fragment.apply {
                arguments = intent.extras
             }, R.id.contentFrame
        )
    }

    private fun configureActionBar() {
        setupActionBar(R.id.toolbar) {
            setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            toolbar_title.text = getString(R.string.titleMovementActivity)
        }
    }

    override fun sendCommand(jsonData: String) {
        viewModel.sendBluetoothCommand(jsonData)
    }
}
