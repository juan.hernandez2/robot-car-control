package com.example.robot_car_control.movementControl.rotation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.robot_car_control.data.CarCommand
import com.example.robot_car_control.data.ControlMode
import javax.inject.Inject

class RotationViewModel @Inject constructor() : ViewModel() {

    private val _carCommand = MutableLiveData<CarCommand>()
    val carCommand: LiveData<CarCommand> = _carCommand

    fun sendAngleSeekBarValue(progressValue: Int) {
        _carCommand.value = CarCommand(ControlMode.ANGLE_POS.mode, progressValue)
    }

}