package com.example.robot_car_control.movementControl.movement

import android.widget.SeekBar
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.robot_car_control.data.CarCommand
import com.example.robot_car_control.data.ControlMode
import com.example.robot_car_control.data.MovementType
import javax.inject.Inject


class MovementViewModel @Inject constructor() : ViewModel() {

    val angularDisplacement: MutableLiveData<String> = MutableLiveData()

    private val _carCommand = MutableLiveData<CarCommand>()
    val carCommand: LiveData<CarCommand> = _carCommand

    fun sendMoveAction(option: Int) {
        _carCommand.value =  CarCommand(ControlMode.MOVE_MODE.mode, option)
    }

    fun sendSpeedSeekBarValue(seekBar: SeekBar, progressValue: Int, fromUser: Boolean) {
        _carCommand.value = CarCommand(ControlMode.SPEED_MODE.mode, progressValue)
    }

    fun sendAngularDisplacement(direction: Int) {
        var angularValue = Math.abs( angularDisplacement.value?.toInt() ?: 0)
        if(direction == MovementType.MOVE_LEFT.type) {
            angularValue *= -1
        }
        _carCommand.value = CarCommand(ControlMode.ANGLE_DIS.mode, angularValue)
        angularDisplacement.value = ""
    }

}