package com.example.robot_car_control.movementControl.movement

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.robot_car_control.databinding.FragmentMovementControlBinding
import com.example.robot_car_control.di.AppComponentHolder
import com.example.robot_car_control.movementControl.BluetoothCommandListener
import com.google.gson.Gson
import javax.inject.Inject

class MovementFragment : Fragment() {

    @Inject
    lateinit var viewModel: MovementViewModel

    private lateinit var viewDataBinding: FragmentMovementControlBinding
    private var bluetoothCommandListener: BluetoothCommandListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is BluetoothCommandListener) {
            bluetoothCommandListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        bluetoothCommandListener = null
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        AppComponentHolder.component.inject(this)
        setDataBinding(inflater, container)
        return viewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addObservers()
    }

    private fun setDataBinding(inflater: LayoutInflater, container: ViewGroup?) {
        viewDataBinding = FragmentMovementControlBinding.inflate(inflater, container, false).apply {
            viewModel = this@MovementFragment.viewModel
            lifecycleOwner = this@MovementFragment.viewLifecycleOwner
        }
    }

    private fun addObservers() {
        viewModel.carCommand.observe(this, Observer {
            bluetoothCommandListener?.sendCommand(Gson().toJson(it))
        })
    }

    companion object {
        @JvmStatic
        fun newInstance() = MovementFragment()
        private val TAG = MovementFragment::class.java.simpleName
    }

}
