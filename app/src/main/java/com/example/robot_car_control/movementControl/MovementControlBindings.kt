package com.example.robot_car_control.movementControl

import androidx.databinding.BindingAdapter
import com.google.android.material.bottomnavigation.BottomNavigationView

@BindingAdapter("app:onNavigationItemSelectedListener")
fun setOnNavigationItemSelectedListener(
        bottomNavigationView: BottomNavigationView,
        onNavigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener?
) {
    bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
}