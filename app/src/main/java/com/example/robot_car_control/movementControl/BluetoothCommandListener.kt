package com.example.robot_car_control.movementControl

interface BluetoothCommandListener {
    fun sendCommand(jsonData: String)
}