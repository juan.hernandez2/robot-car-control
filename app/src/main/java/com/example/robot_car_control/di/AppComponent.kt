package com.example.robot_car_control.di

import com.example.robot_car_control.devices.BluetoothDevicesListFragment
import com.example.robot_car_control.movementControl.modes.ModesFragment
import com.example.robot_car_control.movementControl.MovementControlActivity
import com.example.robot_car_control.movementControl.movement.MovementFragment
import com.example.robot_car_control.movementControl.rotation.RotationFragment
import dagger.Component

@Component
interface AppComponent {

    fun inject(bluetoothDevicesListFragment: BluetoothDevicesListFragment)
    fun inject(movementFragment: MovementFragment)
    fun inject(modesFragment: ModesFragment)
    fun inject(rotationFragment: RotationFragment)

    fun inject(movementControlActivity: MovementControlActivity)
}