package com.example.robot_car_control

import android.app.Application
import com.example.robot_car_control.di.AppComponentHolder
import com.example.robot_car_control.di.DaggerAppComponent

class BaseApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        initAppComponent()
    }

    private fun initAppComponent() {
        AppComponentHolder.component =
                DaggerAppComponent.builder().build()
    }
}